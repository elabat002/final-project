import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  CustomTextField({
    @required this.controller,
    @required this.width,
    @required this.height,
    @required this.hint,
    this.borderColor = Colors.blueAccent,
    this.icon,
    this.obscureText = false,
    this.autofocus = true,
    this.onChanged,
    this.onTap,
    Key key
  }) : super(key: key);

  final TextEditingController controller;
  final double width;
  final double height;
  final Color borderColor;
  final String hint;
  final IconData icon;
  final bool obscureText;
  final bool autofocus;
  final Function onChanged;
  final Function onTap;
  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      height: widget.height,
      decoration: BoxDecoration(
          color: Colors.grey[200],
          border: widget.borderColor != Colors.blueAccent ? Border.all(color: widget.borderColor, width: 2) : Border(),
          borderRadius: BorderRadius.circular(6)),
      child: Center(
        child: TextField(
            autofocus: widget.autofocus,
            controller: widget.controller,
            onChanged: widget.onChanged != null ? (text) => widget.onChanged(text) : null,
            onTap: widget.onTap,
            textAlignVertical: widget.controller.text.isNotEmpty ? TextAlignVertical.center : null,
            obscureText: widget.obscureText,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(left: 8.0),
              border: InputBorder.none,
              hintText: widget.hint,
              hintStyle: TextStyle(fontFamily: "Comfortaa", fontSize: 12),
              icon: widget.icon != null ? 
                Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Icon(widget.icon),
                ) : null,
              suffixIcon: widget.controller.text.isNotEmpty ? IconButton(
                icon: Icon(Icons.clear),
                onPressed: () => widget.controller.clear(),
              )
              : null
            )
        ),
      ),
    );
  }
}
