import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  CustomButton({
    @required this.width,
    @required this.height,
    @required this.boxDecoration,
    @required this.color,
    @required this.text,
    @required this.onTap,
    this.elevation = 0
  });

  final double width;
  final double height;
  final BoxDecoration boxDecoration;
  final Color color;
  final Widget text;
  final Function onTap;
  final double elevation;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: elevation,
      color: color,
      borderRadius: boxDecoration.borderRadius,
      child: InkWell(
        onTap: onTap,
        child: Container(
          width: width,
          height: height,
          decoration: boxDecoration,
          child: Center(
            child: text,
          )
        )
      ),
    );
  }
}