import 'dart:async';
import 'dart:math' as math;

import 'package:app/CustomTextField.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:hb_check_code/hb_check_code.dart';
import 'package:random_string/random_string.dart';

import 'package:app/CustomButton.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _accessCode = '';
  String _mobileNumber = '';
  String _captcha_code = '';
  String _captcha_input = '';
  String _emailReceiver = 'enzolabat@hotmail.fr';
  Timer _timer;
  double _countdown = 30.0;
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _accessCode = _generateNewAccessCode().toString();
    _captcha_code = randomAlpha(6);
    _timer = new Timer.periodic(Duration(milliseconds: 100), (timer) {
      if (_countdown <= 0.1) {
        setState(() {
          _accessCode = _generateNewAccessCode().toString();
          _countdown = 30.0;
        });
      } else
        setState(() => _countdown -= 0.1);
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void _getInput(String input) {
    _captcha_input = input;
  }

  void _onCaptchaTap() {
    showDialog(
        context: context,
        builder: (_) => StatefulBuilder(builder: (context, setState) {
              return AlertDialog(
                  title: Text("Captcha"),
                  content: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        children: [
                          HBCheckCode(
                              height: 50, width: 200, code: _captcha_code),
                          IconButton(
                              icon:
                                  Icon(Icons.refresh, color: Colors.blueAccent),
                              onPressed: () => setState(
                                  () => _captcha_code = randomAlpha(6)))
                        ],
                      ),
                      Padding(padding: EdgeInsets.all(8.0)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CustomTextField(
                            controller: _controller,
                            width: 175,
                            height: 50,
                            hint: '',
                            onChanged: _getInput,
                          ),
                          IconButton(
                              icon: Icon(Icons.arrow_forward_outlined,
                                  color: Colors.blueAccent),
                              onPressed: () {
                                if (_captcha_input.toLowerCase() ==
                                    _captcha_code.toLowerCase()) {
                                  Navigator.pop(context);
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(SnackBar(
                                    content: Text('Valid Code !'),
                                    action: SnackBarAction(
                                      label: 'Undo',
                                      onPressed: () {},
                                    ),
                                  ));
                                } else {
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(SnackBar(
                                    content: Text('Invalid Code !'),
                                    action: SnackBarAction(
                                      label: 'Undo',
                                      onPressed: () {},
                                    ),
                                  ));
                                }
                                setState(() {
                                  _controller.clear();
                                  _captcha_input = '';
                                  _captcha_code = randomAlpha(6);
                                });
                              })
                        ],
                      )
                    ],
                  ));
            }));
  }

  void _onEmailTap() async {
    String username = 'labat002@cougars.csusm.edu';
    String password = 'HNg4WU9cY7IApGk';

    final smtpServer = gmail(username, password);
    // Use the SmtpServer class to configure an SMTP server:
    // final smtpServer = SmtpServer('smtp.domain.com');
    // See the named arguments of SmtpServer for further configuration
    // options.

    // Create our message.
    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    String date = dateFormat.format(DateTime.now());
    String accessCode = _generateNewAccessCode().toString();
    final message = Message()
      ..from = Address(username, 'Enzo Labat')
      ..recipients.add(_emailReceiver)
      ..subject = 'Final Project Access Code 🔑 $date'
      ..html = "<h1>Here is your access code: </h1>\n<p>$accessCode</p>";

    try {
      final sendReport = await send(message, smtpServer);
      print('Message sent: ' + sendReport.toString());
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Message successfully sent !'),
        action: SnackBarAction(
          label: 'Undo',
          onPressed: () {},
        ),
      ));
    } on MailerException catch (e) {
      print('Message not sent.');
      for (var p in e.problems) {
        print('Problem: ${p.code}: ${p.msg}');
      }
    }
  }

  int _generateNewAccessCode() {
    var rnd = new math.Random();
    var next = rnd.nextDouble() * 1000000;
    while (next < 100000) {
      next *= 10;
    }
    return next.toInt();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: Theme.of(context).accentColor,
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Spacer(
                  flex: 2,
                ),
                InkWell(
                  onDoubleTap: () {
                    Clipboard.setData(
                        ClipboardData(text: _accessCode.toString()));
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text('Copied to Clipboard !'),
                      action: SnackBarAction(
                        label: 'Undo',
                        onPressed: () {},
                      ),
                    ));
                  },
                  child: Text(_accessCode,
                      style: TextStyle(
                          fontSize: 30,
                          color: Colors.blueGrey,
                          letterSpacing: 5)),
                ),
                Spacer(),
                Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Container(
                      width: 40,
                      height: 40,
                      child: SfRadialGauge(
                        axes: [
                          RadialAxis(
                            minimum: 0,
                            maximum: 30,
                            showLabels: false,
                            showTicks: false,
                            startAngle: 270,
                            endAngle: 270,
                            isInversed: true,
                            axisLineStyle: AxisLineStyle(
                              color: Colors.transparent,
                              thicknessUnit: GaugeSizeUnit.factor,
                            ),
                            pointers: <GaugePointer>[
                              RangePointer(
                                enableAnimation: true,
                                color: Colors.blueGrey,
                                value: _countdown,
                                width: 0.95,
                                pointerOffset: 0.05,
                                sizeUnit: GaugeSizeUnit.factor,
                              )
                            ],
                          )
                        ],
                      ),
                    ))
              ],
            ),
            Column(
              children: [
                Center(
                  child: CustomButton(
                      width: (MediaQuery.of(context).size.width * 0.914)
                          .roundToDouble(),
                      height: (MediaQuery.of(context).size.height * 0.064)
                          .roundToDouble(),
                      boxDecoration:
                          BoxDecoration(borderRadius: BorderRadius.circular(6)),
                      color: Colors.blueAccent,
                      text: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: Icon(
                              Icons.donut_small_rounded,
                              color: Colors.white,
                            ),
                          ),
                          Text("Fill Captcha",
                              style: TextStyle(
                                  fontFamily: "Comfortaa",
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)),
                        ],
                      ),
                      onTap: _onCaptchaTap),
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                Center(
                  child: CustomButton(
                      width: (MediaQuery.of(context).size.width * 0.914)
                          .roundToDouble(),
                      height: (MediaQuery.of(context).size.height * 0.064)
                          .roundToDouble(),
                      boxDecoration:
                          BoxDecoration(borderRadius: BorderRadius.circular(6)),
                      color: Colors.blueAccent,
                      text: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: Icon(
                              Icons.email,
                              color: Colors.white,
                            ),
                          ),
                          Text("Send email",
                              style: TextStyle(
                                  fontFamily: "Comfortaa",
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)),
                        ],
                      ),
                      onTap: _onEmailTap),
                )
              ],
            ),
          ],
        ));
  }
}
