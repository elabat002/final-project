// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:app/CustomButton.dart';
import 'package:app/CustomTextField.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:app/HomePage.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

void main() {
  testWidgets('Widget HomePage', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
        home: HomePage(
      title: 'Home',
    )));

    expect(find.byType(Scaffold), findsOneWidget);
    expect(find.byType(Column), findsWidgets);
    expect(find.byType(Row), findsWidgets);
    expect(find.byType(Padding), findsWidgets);
    expect(find.byType(CustomButton), findsWidgets);
    expect(find.byType(SfRadialGauge), findsOneWidget);
  });
  testWidgets('Widget CustomButton', (WidgetTester tester) async {
    int counter = 0;

    await tester.pumpWidget(MaterialApp(
        home: CustomButton(
            width: 150,
            height: 75,
            boxDecoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(6))),
            color: Colors.blueAccent,
            text: Text('This is a test'),
            onTap: () => counter++)));

    expect(find.byType(Material), findsOneWidget);
    expect(find.byType(InkWell), findsOneWidget);
    expect(find.byType(Container), findsOneWidget);
    expect(find.byType(Center), findsWidgets);
    expect(find.byType(Text), findsOneWidget);
    expect(find.text('This is a test'), findsOneWidget);

    await tester.tap(find.byType(Material));
    await tester.pump();
    expect(counter, 1);
  });

  testWidgets('Widget CustomTextField', (WidgetTester tester) async {
    TextEditingController controller = TextEditingController();
    await tester.pumpWidget(MaterialApp(
        home: Material(
      child: CustomTextField(
        controller: controller,
        width: 150,
        height: 75,
        hint: 'This is a test',
        icon: Icons.search,
      ),
    )));

    expect(find.byType(Container), findsOneWidget);
    expect(find.byType(TextField), findsOneWidget);
    expect(find.byType(Icon), findsOneWidget);
    expect(find.byIcon(Icons.search), findsOneWidget);
    expect(find.text('This is a test'), findsOneWidget);
    expect(find.byType(IconButton), findsNothing);
  });
}
